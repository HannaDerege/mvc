/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author han
 */
public class LoginView extends JFrame {
    
    private JLabel userName=new JLabel("UserName:");
    
    private JTextField user=new JTextField(60);
    private JLabel Password=new JLabel("Password:");
    private JPasswordField pass=new JPasswordField(60);
    private JButton login=new JButton("Login");
    
    public LoginView(){
        JPanel loginPanel=new JPanel();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          setTitle("login form");
       userName.setBounds(10,100,200,40);
       user.setBounds(110,100,200,30);
       Password.setBounds(10,150,200,40);
       pass.setBounds(110,150,200,30);
       login.setBounds(10,200,100,40);
          setSize(400,400);
          setVisible(true);
          add(userName);
           add(user);
           add(Password);
            add(pass);
             add(login);
             this.add(loginPanel);
    
    }
    public String getUserName(){
         return user.getText().toString();
    }
    public String getPassword(){
        return pass.getText().toString();
    }
    public void setCheckValue(boolean check){
        if(check==true){
        JOptionPane.showMessageDialog(null,"you have logged in successfully");
        }
        else{
            JOptionPane.showMessageDialog(null, "invalid input");
        }
    }
   
    
    public void addCheckListener(ActionListener listener){
        login.addActionListener(listener);
        
    
    }
  

    
    
}

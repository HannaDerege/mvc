/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author han
 */
public class LoginController {
    private LoginView theView;
    private LoginModel theModel;
    
    public LoginController(LoginView theView,LoginModel theModel){
        this.theModel=theModel;
        this.theView=theView;
        
        this.theView.addCheckListener(new CheckListener());
    
    }
    class CheckListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
           String userName,Password;
           
           userName=theView.getUserName();
           Password=theView.getPassword();
           
           boolean a=theModel.checkUser(userName, Password);
          theView.setCheckValue(a);
           
        
        }
    
    
    }
}
